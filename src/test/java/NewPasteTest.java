import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class NewPasteTest {
    private WebDriver driver;

    @Test
    public void newPasteCreating() {
        driver = new ChromeDriver();
        boolean newPasteCreated = new NewPaste(driver).
                openPasteService().
                closePopupCookies().
                addCode().
                setExpiration().
                addTitle().
                createNewPaste().
                checkNewPasteCreation();

        driver.quit();
        driver = null;

        Assert.assertTrue("New paste hasn't been created", newPasteCreated);
    }
}
