import org.checkerframework.checker.units.qual.N;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import javax.lang.model.element.Element;

public class NewPaste {

    private static final String PASTE_SERVICE_URL = "https://pastebin.com/";
    private static String newPasteCode = "Hello from WebDriver";
    private static String expiration = "10 Minutes";
    private static String newPasteTitle = "helloweb";

    private WebDriver driver;

    @FindBy(xpath = ("//span[text()='AGREE']"))
    private List<WebElement> popupCookiesAgreeBtn;

    @FindBy(id = ("postform-text"))
    private WebElement newPasteForm;

    @FindBy(id = ("select2-postform-expiration-container"))
    private WebElement dropdownExpiration;

    @FindBy(id = ("postform-name"))
    private WebElement newPasteTitleForm;

    @FindBy(xpath = ("//*[@class=\"btn -big\"]"))
    private WebElement createNewPaste;

    @FindBy(xpath = ("//*[@class='notice -success -post-view']"))
    private List <WebElement> newPasteCreationNote;

    boolean newPasteCreation;

    public NewPaste(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public NewPaste openPasteService(){
        driver.get(PASTE_SERVICE_URL);
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='wrap']")));
        return this;
    }

    public NewPaste closePopupCookies(){
        if(popupCookiesAgreeBtn.size() > 0){
        popupCookiesAgreeBtn.get(0).click();}
        return this;
    }

    public NewPaste addCode(){
        newPasteForm.sendKeys(newPasteCode);
        return this;
    }

    public NewPaste setExpiration() {
        dropdownExpiration.click();
        WebElement expirationDropdownValue = driver.findElement(By.xpath("//li[text()='" + expiration + "']"));
        expirationDropdownValue.click();
        return this;
    }

    public NewPaste addTitle(){
        newPasteTitleForm.sendKeys(newPasteTitle);
        return this;
    }

    public NewPaste createNewPaste(){
        createNewPaste.click();
        return this;
    }

    public boolean checkNewPasteCreation(){
        if(newPasteCreationNote.size() > 0){
            newPasteCreation = true;
        }
        return newPasteCreation;
    }
}
